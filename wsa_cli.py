import asyncio
import concurrent.futures
import csv
import re
import sys
from typing import List, Tuple
from math import nan

import aiohttp
import click
from bs4 import BeautifulSoup

WSA_URL = 'https://wordsimilarity.com/word-similarity-api'
MAX_CONNECTIONS_DEFAULT = 50
LANG_DEFAULT = 'en'
OUTPUT_FILE_DEFAULT = 'output.csv'


async def comparePair(session, pair: Tuple[str, str], lang: str, csrf_token: str) -> float:
    """Compare two words and returns the similarity using an existing connection"""
    word1, word2 = pair
    payload = {
        'csrf_token': csrf_token,
        'word1': word1,
        'word2': word2,
        'lang': lang,
    }
    async with session.post(WSA_URL, data=payload) as response:
        html = await response.text()
    soup = BeautifulSoup(html, 'html.parser')

    # Similarity is written in the html as 'Similarity: 0.456785 (cosine [...]'
    response_paragraphe = soup.find('p', text=re.compile('Similarity: '))
    if (response_paragraphe is None):
        print(f'Could not analyze words ({word1}, {word2}).', file=sys.stderr)
        return nan
    response_line = response_paragraphe.contents[0].strip()
    similarity = float(response_line.split(' ')[1])

    return similarity


async def comparePairs(pairs: List[Tuple[str, str]], lang: str, max_connections: int) -> List[float]:
    """Establish a connection and asynchronously compare words from a list"""
    conn = aiohttp.TCPConnector(limit=max_connections)
    async with aiohttp.ClientSession(connector=conn) as session:
        # Get the page in order to check supported lang and to get a csrf_token token
        async with session.get(WSA_URL) as response:
            html = await response.text()
        soup = BeautifulSoup(html, 'html.parser')
        csrf_token = soup.find('input', id='csrf_token')['value']

        # Check if lang is supported
        if soup.find('select', attrs={'name': 'lang'}).find('option', value=lang) is None:
            raise ValueError(f'this lang is not supported: {lang}')

        # Wait for the results
        futures = [comparePair(session, p, lang, csrf_token) for p in pairs]
        return await asyncio.gather(*futures)


@click.command()
@click.argument('input_file')
@click.argument('output_file', default=OUTPUT_FILE_DEFAULT)
@click.option('--lang', '-l', default=LANG_DEFAULT, help=f'Language code used for the analysis. Default is \'{LANG_DEFAULT}\'.')
@click.option('--max-connections', '-m', 'max_connections', default=MAX_CONNECTIONS_DEFAULT, help=f'Maximum number of parallel connections. Default is {MAX_CONNECTIONS_DEFAULT}.')
def main(input_file, output_file, lang, max_connections):
    """Reads rows from the given csv file, analyse pairs of words and then save the results"""
    # Try to read the csv file
    try:
        with open(input_file, encoding='utf-8') as input_stream:
            csv_reader = csv.reader(input_stream)
            words = [tuple(row) for row in csv_reader]
    except FileNotFoundError as e:
        print(e, file=sys.stderr)
        return

    # Analysis
    loop = asyncio.get_event_loop()
    try:
        results = loop.run_until_complete(
            comparePairs(words, lang, max_connections))
    except ValueError as e:
        print(e, file=sys.stderr)
        return
    
    # Save the results
    with open(output_file, mode='w', encoding='utf-8') as output_stream:
        output_stream.writelines([f'{row}\n' for row in results])


if __name__ == '__main__':
    main()
