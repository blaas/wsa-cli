# WSA CLI

Python command line interface for [Word Similarity API](https://wordsimilarity.com/word-similarity-api)
This is a quickly developed tool for helping out a friend. Feel free to contact me if you have a question or a suggestion.

## Requirements

* Python 3.8
* Pipenv

## Installation

```Bash
git clone https://gitlab.com/blaas/wsa-cli.git
cd wsa-cli
pipenv install
pipenv run python wsa_cli.py
```

## Guide

### Analysis language

You can change the language used for the analysis with the `--lang` option. Default is 'en' for english.
You can check on [Word Similarity API](https://wordsimilarity.com/word-similarity-api) which languages are covered in the language dropdown.

```Bash
# This will perform the analysis in french
pipenv run python wsa_cli.py input.csv --lang fr
```

### Parallel connections

You can set the maximum number of parallel connection you want with the `--max-connections` option. Default is 50.

```Bash
# This will set the maximum of connections to 20
pipenv run python wsa_cli.py input.csv --max-connections 20
```
